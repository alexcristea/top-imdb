import React from 'react'

export default class ReleaseDateFormatter {

	constructor(releaseDate) {
		this.date = new Date(releaseDate)
	}

	releaseYear() {
		return this.date.getFullYear()
	}

	localized() {
		var options = { year: "numeric", month: "long", day: "numeric" };
		return this.date.toLocaleDateString("en-US", options)
	}
}