
export default class GenresFormatter {

	constructor(genres) {
		this.genres = genres ? [...genres] : []
	}

	toString() {
		return this.genres.map((item) => item.name).join(', ')
	}
}