
const images = {
	logo: require('../resources/img/logo_sqr.png'),
	icons: {
		back: require('../resources/img/icn_back.png'),
		next: require('../resources/img/icn_next.png'),
		discover: require('../resources/img/icn_discover.png'),
		explore: require('../resources/img/icn_explore.png'),
		reviews: require('../resources/img/icn_reviews.png'),
		link: require('../resources/img/icn_link.png'),
	},
	placeholder: require('../resources/img/placeholder.png'),
}

export default images