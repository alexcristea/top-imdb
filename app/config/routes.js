
import { SearchMovies } from '../routes/search-movies'
import PopularMoviesContainer from '../routes/popular-movies'

const routes = {
	popularMovies: {
		component: PopularMoviesContainer,
		navigationBarHidden: true
	},

	searchMovies: {
		component: SearchMovies,
		title: `IMDB`,
		navigationBarHidden: false
	}
}

export default routes