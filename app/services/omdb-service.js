

import React from 'react'

const API = 'https://www.omdbapi.com/'
const itemsPerPage = 10

export default class OMDbService {

	searchMovies(keyword, page) {
		const url = `${API}?v=1&type=movie&s=${keyword}&page=${page}`
		return fetch(url)
			.then((response) => response.json())
			.then((response) => {
				if (response.Response == 'True') {
					return Promise.resolve({
						results: response.Search,
						pagesCount: Math.ceil(response.totalResults / itemsPerPage),
						currentPage: page
					})
				}
				else {
					return Promise.reject(response.Error)
				}
			})
	}

	movieDetails(imdbID) {
		const url = `${API}?v=1&type=movie&i=${imdbID}`
		return fetch(url)
			.then((response) => response.json())
			.then((response) => {
				if (response.Response == 'True') {
					return Promise.resolve(response)
				}
				else {
					return Promise.reject(response.Error)
				}
			})
	}
}