
import React, { PropTypes } from 'react'
import DataGateway from './tmdb-service'

export default class MovieDirectoryService {

	constructor() {

		this.dataGateway = new DataGateway()

		this.keyword = ''
		this.results = []
		this.currentPage = 0
		this.pagesCount = 0
	}

	search(keyword) {
		this.keyword = keyword
		this.results = []
		this.currentPage = 0
		this.pagesCount = 0

		return this.askTheGatewayForMovies()
	}

	loadMore() {

		if (this.hasReachedLastPage()) {
			return Promise.resolve(this.results)
		}

		return this.askTheGatewayForMovies()
	}

	hasReachedLastPage() {
		return (this.currentPage == this.pagesCount)
	}

	askTheGatewayForMovies() {

		if (this.keyword.length < 2) {
			return Promise.resolve(this.results)
		}

		var nextPage = Math.max(1, Math.min(this.currentPage + 1, this.pagesCount))

		return this.dataGateway.searchMovies(this.keyword, nextPage).then((response) => {
			this.results = this.results.concat(response.results)
			this.currentPage = response.currentPage
			this.pagesCount = response.pagesCount

			return Promise.resolve(this.results)
		})
	}
}
