

import React from 'react'

const API_ENDPOINT = "https://api.themoviedb.org/3"
const API_IMAGE = "https://image.tmdb.org/t/p"

const API_KEY = "48fbdac127c2866cf1c755f719d025c9"

export default class TMDbService {

	constructor() {
		this._mapMoviePreviewResponse = this._mapMoviePreviewResponse.bind(this)
		this._mapMoviePreview = this._mapMoviePreview.bind(this)
	}

	searchMovies(keyword, page, includeAdult = false) {
		const url = `${API_ENDPOINT}/search/movie?api_key=${API_KEY}&query=${keyword}&page=${page}&include_adult=${includeAdult}`

		return fetch(url)
			.then(r => r.json())
			.then(r => this._mapMoviePreviewResponse(r))
	}

	movieDetails(movieID) {
		const url = `${API_ENDPOINT}/movie/${movieID}?api_key=${API_KEY}`
		return fetch(url).then(r => r.json())
	}

	genres() {
		const url = `${API_ENDPOINT}/genre/movie/list?api_key=${API_KEY}`
		return fetch(url).then(r => r.json())
	}

	_mapMoviePreviewResponse(r) {
		return {
			currentPage: r.page,
			pagesCount: r.total_pages,
			results: r.results.map((item) => this._mapMoviePreview(item))
		}
	}
	_mapMoviePreview(item) {
		return {
			id: item.id,
			title: item.title,
			releaseDate: item.release_date,
			poster: this._buildThumbnailPosterUrl(item.poster_path)
		}
	}

	_buildThumbnailBackdropUrl(path) {
		const small = '/w300'
		return `${API_IMAGE}${small}${path}`
	}

	_buildThumbnailPosterUrl(path) {

		if (path == null) {
			return 'N/A'
		}

		const small = '/w154'
		return `${API_IMAGE}${small}${path}`
	}
}