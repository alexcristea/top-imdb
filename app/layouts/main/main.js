
import React, { Component } from 'react'
import { NavigatorIOS } from 'react-native'

import routes from '../../config/routes'
import styles from './styles'

export default class MainLayout extends Component {

	render() {
		return (
			<NavigatorIOS
				style={styles.container}
				barTintColor='yellow'
				initialRoute={routes.searchMovies} />
		)
	}
}