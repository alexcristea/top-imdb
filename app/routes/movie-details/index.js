
import MovieDetails from './movie-details'
import MovieDetailsContainer from './movie-details-container'

export default MovieDetailsContainer
export { MovieDetails }
