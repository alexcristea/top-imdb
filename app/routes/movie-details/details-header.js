
import React, { Component, PropTypes } from 'react'
import { StyleSheet, View, Text } from 'react-native'
import ReleaseDateFormatter from '../../lib/release-date-formatter'

export default class DetailsHeader extends Component {

	constructor(props) {
		super(props)
		this.renderTagline = this.renderTagline.bind(this)
	}

	render() {

		const { release_date, runtime, title, tagline} = this.props.movie
		const formatter = new ReleaseDateFormatter(release_date)

		return (
			<View style={styles.header}>
				<Text style={styles.subtitle}>{formatter.releaseYear()} · {runtime} min</Text>
				<Text style={styles.title}>{title}</Text>
				{this.renderTagline()}
			</View>
		)
	}

	renderTagline() {
		const { tagline } = this.props.movie
		
		return tagline ? <Text style={styles.tagline}>{tagline}</Text> : <View/>
	}
}

DetailsHeader.propTypes = {
	movie: PropTypes.object.isRequired
}

const styles = StyleSheet.create({
	header: {
		padding: 8
	},

	title: {
		fontSize: 22,
		marginBottom: 8
	},

	tagline: {
		fontSize: 18,
		marginBottom: 8
	},

	subtitle: {
		fontSize: 15,
		color: 'lightgray',
		marginBottom: 8
	}
})