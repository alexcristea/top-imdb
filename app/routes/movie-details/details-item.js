
import React, { Component, PropTypes } from 'react'
import { StyleSheet, View, Text } from 'react-native'

export default class DetailsItem extends Component {

	render() {
		return (
			<View style={styles.item}>
				<Text style={styles.label}>{this.props.label}</Text>
				<Text style={styles.value}>{this.props.value}</Text>
			</View>
		)
	}
}

DetailsItem.propTypes = {
	label: PropTypes.string.isRequired,
	value: PropTypes.any
}

const styles = StyleSheet.create({
	item: {
		padding: 8
	},

	label: {
		fontSize: 15,
		color: 'orange',
		marginBottom: 4
	},

	value: {
		fontSize: 18
	}
})