import React, { Component } from 'react'
import { StyleSheet, View, ScrollView, Text, Image, ActivityIndicator } from 'react-native'

import TMDbService from '../../services/tmdb-service'
import ReleaseDateFormatter from '../../lib/release-date-formatter'
import GenresFormatter from '../../lib/genres-formatter'

import { UISectionHeader, UISeparator, UIIconRow, Poster } from '../../components/ui'

import DetailsItems from './details-item'
import DetailsHeader from './details-header'

import MovieWebsite from '../../components/movie-website'

import images from '../../config/images'

export default class MovieDetails extends Component {

	constructor(props) {
		super(props)

		this.api = new TMDbService()
		this.state = {
			isLoading: false,
			movie: this.props.movie
		}

		this.handleResponse = this.handleResponse.bind(this)
		this.handleError = this.handleError.bind(this)
		this.goToWebsite = this.goToWebsite.bind(this)
	}


	componentDidMount() {
		this.setState({ isLoading: true })
		this.api.movieDetails(this.props.movie.id).then(this.handleResponse).catch(this.handleError)
	}

	handleResponse(response) {
		this.setState({ isLoading: false, movie: response })
	}

	handleError(error) {
		this.setState({ isLoading: false })
	}

	goToWebsite() {

		console.log(this.state.movie)

		this.props.navigator.push({
			component: MovieWebsite,
			title: this.state.movie.title,
			passProps: {
				homepage: this.state.movie.homepage
			}
		})
	}

	render() {
		return (
			<ScrollView style={styles.container}>
				<Poster style={styles.poster} poster={this.props.movie.poster} />
				{this.renderBodyContent()}
			</ScrollView>
		)
	}

	renderBodyContent() {
		if (this.state.isLoading) {
			return <ActivityIndicator style={styles.progress} animating={true} />
		}
		else {
			return this.renderMovieDetails()
		}
	}

	renderMovieDetails() {
		const { movie } = this.state
		const { release_date, runtime, overview, genres } = movie

		const dateFormatter = new ReleaseDateFormatter(release_date)
		const genresFormatter = new GenresFormatter(genres)

		return (
			<View>
				<DetailsHeader movie={movie} />
				<UISeparator />

				<DetailsItems label='Overview' value={overview} />
				<UISeparator />

				<UISectionHeader label="Info">
					<DetailsItems label='Release date' value={dateFormatter.localized()} />
					<UISeparator />
					<DetailsItems label='Runtime' value={`${runtime} min`} />
					<UISeparator />
					<DetailsItems label='Genres' value={genresFormatter.toString()} />
				</UISectionHeader>

				<UISectionHeader label="Explore">
					<UIIconRow iconSource={images.icons.reviews} text="Reviews" />
					<UISeparator />
					<UIIconRow iconSource={images.icons.discover} text="Discover new movies" />
					<UISeparator />
					<UIIconRow iconSource={images.icons.explore} text="Website" onPress={this.goToWebsite} />
				</UISectionHeader>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	poster: {
		flex: 1,
		height: 471
	},
	progress: {
		padding: 16
	}
})