
import PopularMoviesContainer from './popular-movies-container'
import PopularMovies from './popular-movies'

export default PopularMoviesContainer
export { PopularMovies }