
import React, { Component } from 'react'
import { View, Image, ActivityIndicator } from 'react-native'

import styles from './styles'
import images from '../../config/images'

export default class PopularMovies extends Component {

	render() {
		return (
			<View style={styles.container}>
				<Image style={styles.logo} source={images.logo} />
				<ActivityIndicator style={styles.loader} animating={true} />
			</View>
		)
	}
}
