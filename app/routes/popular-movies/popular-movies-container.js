
import React, { Component } from 'react'
import PopularMovie from './popular-movies'

export default class PopularMovieContainer extends Component {

	constructor(props) {
		super(props)
		this.state = {
			isLoading: true
		}

		this.handleDidLoadData = this.handleDidLoadData.bind(this)
		this.timer = setTimeout(this.handleDidLoadData, 3000)
	}

	componentWillUnmount() {
		clearInterval(self.timer)
	}

	handleDidLoadData() {
		console.log("Hello!")
		this.setState({ isLoading: false })
	}

	render() {
		if (this.state.isLoading) {
			return <PopularMovie />
		}
		else {
			return null
		}

	}
}