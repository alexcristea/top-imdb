import React, { Component, PropTypes } from 'react';
import { StyleSheet, View, Text, ListView, ActivityIndicator, NetInfo } from 'react-native';
import { debounce } from 'lodash'

import { UISearchBox, UISeparator } from '../../components/ui'

import MovieRow from './movie-row'
import MovieDetails from '../movie-details'

import MovieDirectoryService from '../../services/movie-directory-service'

export default class MovieListScene extends Component {

	constructor(props) {
		super(props)

		this.movieDirectory = new MovieDirectoryService()

		this.state = {
			isConnected: true,
			isLoading: false,
			hasError: false,
			error: null,
			dataSource: new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
		}

		this.renderFooter = this.renderFooter.bind(this)
		this.renderRow = this.renderRow.bind(this)
		this.renderSeparator = this.renderSeparator.bind(this)

		this.handleTextChange = this.handleTextChange.bind(this)
		this.handleEndReached = this.handleEndReached.bind(this)
		this.handleConnectionInfoChange = this.handleConnectionInfoChange.bind(this)

		this.handleResponseData = this.handleResponseData.bind(this)
		this.handleResponseError = this.handleResponseError.bind(this)

		this.debouncedMovieSearch = debounce(this.debouncedMovieSearch.bind(this), 300)
	}

	componentDidMount() {
		NetInfo.isConnected.addEventListener('change', this.handleConnectionInfoChange)
		NetInfo.isConnected.fetch().then(isConnected => this.handleConnectionInfoChange(isConnected))
	}

	componentWillUnmount() {
		NetInfo.isConnected.removeEventListener('change', this.handleConnectionInfoChange)
	}

	handleConnectionInfoChange(isConnected) {
		this.setState({ isConnected: isConnected })
	}

	// MARK: - Handle actions

	handleTextChange(keyword) {

		if (this.isOffline()) {
			return
		}

		this.prepareStateForNewSearch(keyword)
		this.debouncedMovieSearch()
	}

	isOffline() {
		return this.state.isConnected == false
	}

	prepareStateForNewSearch(keyword) {
		this.setState({
			keyword: keyword,
			isLoading: true,
			hasError: false,
			error: null,
			dataSource: this.state.dataSource.cloneWithRows([])
		})
	}

	debouncedMovieSearch() {
		this.movieDirectory
			.search(this.state.keyword)
			.then(this.handleResponseData)
			.catch(this.handleResponseError)
	}

	handleEndReached() {

		if (this.isLoading()) {
			return
		}

		this.setState({
			isLoading: true
		})

		this.movieDirectory
			.loadMore()
			.then(this.handleResponseData)
			.catch(this.handleResponseError)
	}

	isLoading() {
		return this.state.isLoading
	}

	handleResponseData(results) {
		this.setState({
			isLoading: false,
			dataSource: this.state.dataSource.cloneWithRows(results)
		})
	}

	handleResponseError(error) {
		this.setState({
			isLoading: false,
			hasError: true,
			error: error
		})
	}

	// MARK: - Main render func

	render() {
		return (
			<View style={styles.container}>
				<UISearchBox placeholder="Movie title, keyword, etc" onTextChange={this.handleTextChange} />
				<ListView
					style={styles.list}
					automaticallyAdjustContentInsets={false}
					enableEmptySections={true}
					dataSource={this.state.dataSource}
					renderRow={this.renderRow}
					renderFooter={this.renderFooter}
					renderSeparator={this.renderSeparator}
					onEndReached={this.handleEndReached}
					onEndReachedThreshold={0} />
			</View>
		)
	}

	// MARK: - Render functions

	renderRow(rowData) {
		return <MovieRow movie={rowData} onPress={() => this.goToMovieDetails(rowData)} />
	}

	goToMovieDetails(movie) {
		this.props.navigator.push({
			component: MovieDetails,
			title: movie.title,
			passProps: { movie: movie }
		})
	}

	renderSeparator(sectionID, rowID, adjacentRowHighlighted) {
		const key = `${sectionID}-${rowID}`
		return <UISeparator key={key} />
	}

	renderFooter() {

		if (this.state.isLoading) {
			return <ActivityIndicator style={styles.activityIndicator} animating={true} />
		}

		if (this.state.isConnected == false) {
			return <Text style={styles.messageIndicator}>Check your internet connection.</Text>
		}

		if (this.state.hasError) {
			return <Text style={styles.messageIndicator}>{this.state.error}</Text>
		}

		if (this.state.dataSource.getRowCount() == 0) {
			return <Text style={styles.messageIndicator}>Search for your movie.</Text>
		}

		return <View />
	}
}

MovieListScene.propTypes = {
	navigator: PropTypes.object.isRequired,
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		marginTop: 64
	},
	list: {
		flex: 1
	},
	activityIndicator: {
		padding: 16
	},
	messageIndicator: {
		padding: 16,
		fontSize: 17,
		textAlign: 'center'
	}
})