
import React, { Component, PropTypes } from 'react'
import { StyleSheet, View, Image, Text, TouchableHighlight } from 'react-native'

import { Poster } from '../../components/ui'
import ReleaseDateFormatter from '../../lib/release-date-formatter'

export default class MovieRow extends Component {

	render() {

		const {poster, title, releaseDate } = this.props.movie
		const formatter = new ReleaseDateFormatter(releaseDate)

		return (
			<TouchableHighlight style={styles.row} onPress={this.props.onPress} underlayColor={theme.underlayColor}>
				<View style={styles.container}>
					<Poster style={styles.poster} poster={poster} />
					<View style={styles.details}>
						<Text style={styles.title}>{title}</Text>
						<Text style={styles.year}>{formatter.releaseYear()}</Text>
					</View>
				</View>
			</TouchableHighlight>
		)
	}
}

MovieRow.propTypes = {
	movie: PropTypes.object.isRequired,
	onPress: PropTypes.func
}


const theme = {
	underlayColor: '#EEEEEE',
}

const styles = StyleSheet.create({

	row: {
		flex: 1
	},
	container: {
		flex: 1,
		padding: 8,
		flexDirection: 'row'
	},
	poster: {
		height: 100,
		width: 68
	},
	details: {
		flex: 1,
		paddingLeft: 8
	},
	title: {
		fontSize: 18
	},
	year: {
		color: 'lightgray',
		paddingTop: 8,
		fontSize: 15,
	}
});