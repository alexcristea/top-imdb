
import SearchMovies from './search-movies'
import SearchMoviesContainer from './search-movies-container'

export default SearchMoviesContainer
export { SearchMovies }