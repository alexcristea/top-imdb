
import React, { Component } from 'react'
import { StyleSheet, Animated, View, Image } from 'react-native'

import images from '../../config/images'

const FADEIN_DURATION = 300

export default class Poster extends Component {

	constructor(props) {
		super(props)

		this.state = {
			opacity: new Animated.Value(0)
		}

		this.style = [styles.poster, this.props.style, { opacity: this.state.opacity }]
		this.onLoad = this.onLoad.bind(this)
	}

	onLoad() {
		Animated.timing(this.state.opacity, {
			toValue: 1,
			duration: FADEIN_DURATION
		}).start()
	}

	render() {
		if (this.props.poster == 'N/A') {
			return this.renderPlaceholder()
		}
		else {
			return this.renderFromURI()
		}
	}

	renderPlaceholder() {
		return <Animated.Image style={this.style} onLoad={this.onLoad} source={images.poster} />
	}

	renderFromURI() {
		const poster = { uri: this.props.poster }
		return <Animated.Image style={this.style} onLoad={this.onLoad} source={poster} />
	}
}

const styles = StyleSheet.create({
	poster: {
		width: undefined
	}
})