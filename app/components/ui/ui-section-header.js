import React, { Component, PropTypes } from 'react'
import { StyleSheet, View, Text } from 'react-native'


export default class UISectionHeader extends Component {

	render() {

		const {label} = this.props

		return (
			<View style={styles.container}>
				<Text style={styles.label}>{label}</Text>
				<View style={styles.body}>{this.props.children}</View>
			</View>
		)
	}
}

UISectionHeader.propTypes = {
	label: PropTypes.string.isRequired
}

const styles = StyleSheet.create({
	container: {
		marginBottom: 8
	},
	body: {
		backgroundColor: '#FAFAFA',
		borderTopWidth: 1,
		borderBottomWidth: 1,
		borderTopColor: '#EEEEEE',
		borderBottomColor: '#EEEEEE',

	},
	label: {
		padding: 8,
		marginTop: 32,
		fontSize: 20
	}
})