
import React, { Component, PropTypes } from 'react'
import { StyleSheet, View, TextInput } from 'react-native'

export default class UISearchBox extends Component {

	constructor(props) {
		super(props)
		this.handleTextChange = this.handleTextChange.bind(this)
	}

	handleTextChange(text) {
		text = text.toLowerCase().trim()
		if (text.length >= 2) {
			this.props.onTextChange(text)
		}
		else {
			this.props.onTextChange('')
		}
	}

	render() {
		return (
			<TextInput
				style={styles.searchBox}
				placeholder={this.props.placeholder}
				onChangeText={this.handleTextChange} />
		)
	}
}

UISearchBox.propTypes = {
	placeholder: PropTypes.string,
	onChangeText: PropTypes.func
}

const styles = StyleSheet.create({
	searchBox: {
		height: 40,
		fontSize: 12,
		borderWidth: 1,
		borderColor: 'lightgray',
		borderRadius: 10,
		margin: 8,
		paddingLeft: 8,
		paddingRight: 8
	}
})