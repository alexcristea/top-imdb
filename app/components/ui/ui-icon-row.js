
import React, { Component, PropTypes } from 'react'
import { StyleSheet, View, Image, Text, TouchableHighlight } from 'react-native'

import images from '../../config/images'

export default class UIIconRow extends Component {

	render() {
		return (
			<TouchableHighlight onPress={this.props.onPress} underlayColor={theme.underlayColor}>
				<View style={styles.contrainer}>
					<Image style={styles.icon} resizeMode="center" resizeMethod="resize" source={this.props.iconSource} />
					<Text style={styles.text}>{this.props.text}</Text>
					<Image style={styles.accesory} resizeMode="center" resizeMethod="resize" source={images.icons.next} />
				</View>
			</TouchableHighlight>
		)
	}
}

const theme = {
	underlayColor: '#EEEEEE',
}

const styles = StyleSheet.create({
	contrainer: {

		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		padding: 8
	},

	icon: {
		width: 20,
		height: undefined,
		alignSelf: 'stretch',
		marginRight: 8
	},

	accesory: {
		height: undefined,
		width: 10,
		alignSelf: 'stretch',
		marginLeft: 8
	},

	text: {
		flex: 1,
		textAlignVertical: 'center',
		fontSize: 17
	}
})