
import React, { Component } from 'react'
import { StyleSheet, View } from 'react-native'

export default class UISeparator extends Component {

	render() {
		return (
			<View style={styles.separator} />
		)
	}
}

const styles = StyleSheet.create({
	separator: {
		flex: 1,
		height: 1,
		marginLeft: 8,
		backgroundColor: '#EEEEEE',
	}
})