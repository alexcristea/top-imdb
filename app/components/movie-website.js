
import React, { Component } from 'react'
import { StyleSheet, WebView } from 'react-native'


export default class MovieWebsite extends Component {

	render() {
		return (
			<WebView style={styles.container} source={{ uri: this.props.homepage }} />
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		marginTop: 20
	}
})