/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react'
import { AppRegistry } from 'react-native'
import MainLayout from './app/layouts/main'

export default class TopIMDB extends Component {
  render() {
    return <MainLayout />
  }
}

AppRegistry.registerComponent('TopIMDB', () => TopIMDB);
